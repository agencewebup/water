<?php

namespace water\views;

class Card extends \water\views\View
{
    /**
     * Liste des objets de la grille
     * @var array
     */
    private $items = array();

    /**
     * Class css/js du containeur
     * @var string
     */
    private $class = 'grid-5';

    /**
     * Ajoute un objet
     * @param int    $title     Titre de la cart
     * @param string $url       Url
     */
    public function addItem($id, $url)
    {
        $this->items[] = array(
            'title' => $id,
            'url' => $url,
            'actions' => array()
        );
    }

    /**
     * Ajoute une action à la derniere ligne ajouté
     * @param string  $title   Titre de la l'action
     * @param string  $url     Url
     * @param string  $icon    Icone
     * @param string  $class   Classe JS/CSS
     */
    public function addAction($title, $url, $icon, $class = null)
    {
        $lastIndex = count($this->items) - 1;

        $this->items[$lastIndex]['actions'][] = array(
            'title' => $title,
            'url' => $url,
            'icon' => $icon,
            'class' => $class
        );
    }

    /**
     * Set une classe sur le containeur
     * @param string $class classs css/js
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    public function render()
    {
        return $this->element(
            'templates/elements/card.phtml',
            array('items' => $this->items, 'class' => $this->class)
        );
    }
}
