<?php

namespace water\views;

class Grid extends \water\views\View
{
    /**
     * Liste des objets de la grille
     * @var array
     */
    private $items = array();

    /**
     * Url pour sauvegader l'ordre des objets
     * @var string
     */
    private $orderUrl;

    /**
     * Ajoute un objet
     * @param int    $id        Id de l'objet, utiliser pour le sort
     * @param string $imageUrl  Url de l'image représentant l'objet
     * @param string $updateUrl Url pour modifier l'objet
     * @param string $deleteUrl Url pour supprimer l'objet
     */
    public function addItem($id, $imageUrl, $updateUrl, $deleteUrl)
    {
        $this->items[] = array(
            'id' => $id,
            'imageUrl' => $imageUrl,
            'updateUrl' => $updateUrl,
            'deleteUrl' => $deleteUrl,
        );
    }

    /**
     * Set l'url pour sauvegader l'ordre des objets
     * @param string $url Url
     */
    public function setOrderUrl($url)
    {
        $this->orderUrl = $url;
    }

    public function render()
    {
        return $this->element(
            'templates/elements/grid.phtml',
            array('items' => $this->items, 'orderUrl' => $this->orderUrl)
        );
    }
}
