<?php

namespace water\views;

class Menu extends \water\views\View
{
    /**
     * Element du menu
     * @var array
     */
    private $menu = array();

    /**
     * Clé du menu courant
     * @var string
     */
    private $current;

    /**
     * Ajout un item au menu
     * @param string $title   Titre
     * @param string $url     Url
     * @param string $icon    Icone fontawesome
     * @param string $current Current key
     */
    public function add($title, $url, $icon, $current = null)
    {
        if ($current === null) {
            $current = count($this->menu);
        }
        
        $this->menu[] = array(
            'title' => $title,
            'url' => $url,
            'icon' => $icon,
            'current' => $current,
            //'submenu' => array()
        );

        return $this;
    }

    // public function addSubmenu($title, $url, $icon)
    // {
    //     $this->menu[$this->lastIndex]['submenu'][] = array('title' => $title, 'url' => $url,'icon' => $icon);

    //     return $this;
    // }

    /**
     * Set la clé du menu courant
     * @param string $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    public function render()
    {
        return $this->element(
            'templates/elements/menu.phtml',
            array('menu' => $this->menu, 'current' => $this->current)
        );
    }
}
