<?php

namespace water\views;

class Tab extends \water\views\View
{
    /**
     * Onglets
     * @var array
     */
    private $tabs = array();

    /**
     * Clé de l'onglet courant
     * @var string
     */
    private $current;

    /**
     * Ajoute un onglet
     * @param string $title   Titre
     * @param string $url     Url
     * @param string $current Current key
     */
    public function add($title, $url, $current = null)
    {
        if ($current === null) {
            $current = count($this->tabs);
        }

        $this->tabs[] = array(
            'title' => $title,
            'url' => $url,
            'current' => $current
        );
    }

    /**
     * Set la clé du menu courant
     * @param string $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    public function render()
    {
        return $this->element(
            'templates/elements/tab.phtml',
            array('tabs' => $this->tabs, 'current' => $this->current)
        );
    }
}
