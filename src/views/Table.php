<?php

namespace water\views;

class Table extends \water\views\View
{
    /**
     * Class css/js du tableau
     * @var string
     */
    private $class = 'js-water-datatable';

    /**
     * Contient les informations pour construire le header du tableau
     * @var array
     */
    private $headers = array();

    /**
     * Contient le contenu du tableau
     * @var array
     */
    private $rows = array();

    /**
     * Set la classe de <table>
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * Ajout une colonne
     * @param string $title Titre de la colonne
     * @param string $type  Type de la colonne ('text', 'number', 'bool', 'actions')
     * @param string $key   Clé du contenu à utiliser pour récupérer
     */
    public function addHeader($title, $type, $key = null)
    {
        $thClass = '';
        $tdClass = '';

        if ($key == null) {
            $key = count($this->headers);
        }

        if ($type == 'text') {
            //
        } elseif ($type == 'number') {
            $tdClass = 'txtright';

        } elseif ($type == 'bool') {
            $thClass = 'w10';
            $tdClass = 'txtcenter';

        } elseif ($type == 'actions') {
            $key = 'actions';
            $tdClass = 'actions';
        }

        $this->headers[$key] = array('title' => $title, 'type' => $type, 'class' => $thClass, 'tdClass' => $tdClass);
    }

    /**
     * Ajout une ligne
     * @param array $content contient les données des colonne
     */
    public function addRow($content)
    {
        foreach ($content as $key => $value) {
            if (! is_array($value)) {
                $content[$key] = array($value);
            }
        }

        $this->rows[] = $content; //array('cols' => $content, 'class' => $class);
    }

    /**
     * Ajoute une action à la derniere ligne ajouté
     * @param string  $title   Titre de la l'action
     * @param string  $url     Url
     * @param string  $icon    Icone
     * @param string  $class   Classe
     * @param bool    displayTitle si false ca affiche seulement l'icone et pas le title
     */
    public function addAction($title, $url, $icon, $class = null, $displayTitle = true)
    {
        $lastIndex = count($this->rows) - 1;
        if (! isset($this->rows[$lastIndex]['actions'])) {
            $this->rows[$lastIndex]['actions'] = array('');
        }

        $fullTitle = '';

        if ($displayTitle) {
            $fullTitle = $title;
        }

        $this->rows[$lastIndex]['actions'][0] .= '<a href="'.$url.'" class="'.$class.'" title="'.$title.'"><i class="fa '.$icon.'"></i>'.$fullTitle.'</a> ';
    }

    /**
     * Format le contenu d'une cellule
     * @param  string $content contenu à formater
     * @param  string $type    type de la cellule
     * @return string          string formaté
     */
    public function format($content, $type)
    {
        if ($type === 'bool') {
            $content = $content ? '<span style="color:green">&#10004</span>' : '<span style="color:red">&#10008</span>';
        }

        return $content;
    }

    public function render()
    {
        return $this->element(
            'templates/elements/table.phtml',
            array('class' => $this->class, 'headers'=> $this->headers, 'rows' => $this->rows)
        );
    }
}
