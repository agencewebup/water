<?php

namespace water\views;

class Water extends \water\views\View
{
    /**
     * Menu
     * @var Menu
     */
    public $menu;

    private $waterBaseUrl;

    /**
     * Initialisation des variables de Water
     */
    public function init()
    {
        $this->waterBaseUrl = '/water';
        $this->setLayout('templates/layouts/default.phtml');

        $waterHelper = new \water\views\helpers\Water();

        $this->setHelper('water', $waterHelper);

        $this->menu = new \water\views\Menu();
    }

    /**
     * Utilise la vue login pour le rendu
     * @param  string $getChallengeUrl   Url pour récuperer le challenge de gandalf
     * @param  string $checkChallengeUrl Url pour check le challende de gandalf
     * @param  string $loginRedirectUrl  Url de la page à afficher si l'authentification réussi
     */
    public function useLoginView($getChallengeUrl, $checkChallengeUrl, $loginRedirectUrl)
    {
        $this->setTemplate('templates/auth/login.phtml');
        $this->setLayout('templates/layouts/login.phtml');

        $this->set('getChallengeUrl', $getChallengeUrl);
        $this->set('checkChallengeUrl', $checkChallengeUrl);
        $this->set('loginRedirectUrl', $loginRedirectUrl);
    }

    /**
     * Set le titre et l'url
     * @param array $title Variable
     */
    public function setWaterVars($vars)
    {
        $default = array(
            'headerTitle' => '',
            'headerUrl' => '',
            'logoutUrl' => '',
            'menu' => $this->menu
        );

        $this->set('water', array_merge($default, $vars));
    }

    public function render()
    {
        return parent::render();
    }

    public function water($url)
    {
        return $this->waterBaseUrl . $url;
    }
}
