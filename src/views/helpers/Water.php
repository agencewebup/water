<?php

namespace water\views\helpers;

class Water
{
    public function title($title)
    {
        return '<h2>'.$title.'</h2>';
    }

    public function boxOpen()
    {
        return '<section class="box">';
    }

    public function boxClose()
    {
        return '</section>';
    }

    public function boxTitle($title)
    {
        return '<h3 class="box__title">'.$title.'</h3>';
    }

    public function boxActionsOpen()
    {
        return '<section class="box__actions">';
    }

    public function boxActionsClose()
    {
        return '</section>';
    }

    public function boxContentOpen()
    {
        return '<section class="box__content">';
    }

    public function boxContentClose()
    {
        return '</section>';
    }

    public function boxFooterOpen()
    {
        return '<footer class="box__footer>';
    }

    public function boxFooterClose()
    {
        return '</footer>';
    }
}
