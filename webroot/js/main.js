$(function () {

    $('.menu-btn').menuPannel();

    /* ----------------------------- */
    /* == flash */
    /* ----------------------------- */

    $('.js-alert').each(function(index, element) {
        var flash = $(element);

        if (flash.hasClass('success')) {
            toastr.success(flash.text());
        }
        else if (flash.hasClass('warning')) {
            toastr.warning(flash.text());
        }
        else if (flash.hasClass('error')) {
            toastr.error(flash.text());
        }
        else if (flash.hasClass('info')) {
            toastr.info(flash.text());
        }

        flash.remove();
    });

    /* ----------------------------- */
    /* == datatables */
    /* ----------------------------- */

    $.extend($.fn.dataTable.defaults,  {
        language: {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "autoWidth": false
    });

    $('.js-water-datatable').DataTable();

    /* ----------------------------- */
    /* == datagrids */
    /* ----------------------------- */

    $('.js-water-datagrid').sortable({
        handle: ".js-water-datagrid-drag",
        placeholder: "dotted",
        update: function() {
            $.post(
                $(this).data('sort'),
                {
                    'sort': $(this).sortable('toArray', { attribute: "data-id" } )
                }
            );
        }
    });

    /* ----------------------------- */
    /* == date picker */
    /* ----------------------------- */

    $('.js-water-date').pickadate({
        format: 'dd/mm/yyyy',
        formatSubmit: 'dd/mm/yyyy'
    });

    $('.js-water-time').pickatime({
        interval: 60,
        format: 'HH:i',
        formatSubmit: 'HH:i',
        formatLabel: 'HH:i'
    });

    /* ----------------------------- */
    /* == confirm delete */
    /* ----------------------------- */

    $('.js-water-confirm-delete').click(function(event) {
        if (! confirm('Etes-vous sûr de vouloir supprimer ?')) {
            event.preventDefault();
        }
    });

    /* ----------------------------- */
    /* == select2 - completion */
    /* ----------------------------- */

    $.extend($.fn.dataTable.defaults,  {
       language: "fr"
    });

});
