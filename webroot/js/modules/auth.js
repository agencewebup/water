var Auth = (function ($, CryptoJS) {

    var loginForm;
    var usernameField;
    var passwordField;
    var errorLabel;
    var errorClass;
    var getChallengeUrl;
    var checkChallengeUrl;
    var redirectUrl;

    function encode(challenge, password) {
        var hash = CryptoJS.SHA1(password);
        var encrypted = CryptoJS.HmacSHA256(challenge, hash.toString());

        return encrypted.toString();
    }

    function displayError() {
        errorLabel.text("Nom d'utilisateur ou mot de passe incorrect");
        errorLabel.addClass(errorClass);
    }

    var checkChallenge = function (username, challenge) {
        $.ajax({
            type: 'POST',
            url: checkChallengeUrl,
            data: {
                username: username,
                challenge: challenge
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    window.location.href = redirectUrl;
                } else {
                    displayError();
                }
            },
            error: function (data) {
                displayError();
            }
        });
    };

    var getChallenge = function (username, password) {
        $.ajax({
            type: 'POST',
            url: getChallengeUrl,
            data: {
                username: username
            },
            dataType: 'json',
            success: function (data) {
                if (data !== null && data.challenge) {
                    var challenge = encode(data.challenge, password);
                    checkChallenge(username, challenge);
                } else {
                    displayError();
                }
            },
            error: function (data) {
                displayError();
            }
        });
    };

    var submit = function (event) {
        event.preventDefault();

        getChallenge(usernameField.val(), passwordField.val());

        return false;
    };

    var init = function () {
        loginForm = $('#js-auth-form');
        usernameField = $('#js-auth-form input[name="username"]');
        passwordField = $('#js-auth-form input[name="password"]');
        errorLabel = $('#js-auth-error');
        errorClass = 'f-error';

        getChallengeUrl = loginForm.attr('data-get');
        checkChallengeUrl = loginForm.attr('data-check');
        redirectUrl = loginForm.attr('data-redirect');

        loginForm.on('submit', submit);
    };

    return {
        init: init
    };

})(jQuery, CryptoJS);
