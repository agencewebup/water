(function ( $, window, document, undefined ) {

    var pluginName = "menuPannel",
        defaults = {
            propertyName: "value"
        };

    function Plugin( element, options ) {
        this.element = element;


        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();

    }


    var _currentTitle;

    Plugin.prototype.bindEvents = function () {

        $(this.element).on('click', this.toggleMenu);
        $('.navigation a').hover(this.showTooltip, this.hideTooltip);

    };


    Plugin.prototype.toggleMenu = function () {
        $('.navigation').toggleClass('navigation--open');

        if($('.navigation').hasClass('navigation--open')) {
            sessionStorage.setItem("menu", "open");
        } else {
            sessionStorage.setItem("menu", "closed");
        }
    };


    Plugin.prototype.init = function () {

        this.bindEvents();

        if(sessionStorage.getItem("menu") == 'open') {
            $('.navigation').addClass('navigation--open');
        } else if(sessionStorage.getItem("menu") == 'closed') {
            $('.navigation').removeClass('navigation--open');
        } else {
            sessionStorage.setItem("menu", "open");
            $('.navigation').addClass('navigation--open');
        }

    };


    Plugin.prototype.showTooltip = function () {

        if($('.navigation').hasClass('navigation--open')) {
            return;
        };


        _currentTitle = $(this).attr('title');


    };

    Plugin.prototype.hideTooltip = function () {

        $(this).attr('title', _currentTitle);
        $(this).find('.navigation__tooltip').remove();
    };

    $.fn[pluginName] = function ( options ) {

        return this.each(function () {
            if ( !$.data(this, "plugin_" + pluginName )) {
                $.data( this, "plugin_" + pluginName,
                    new Plugin( this, options ));
            }
        });
    }

})( jQuery, window, document );